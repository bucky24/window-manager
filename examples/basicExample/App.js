import React from 'react';
import Manager from '@bucky24/window-manager'

import styles from './styles.css';

import Component from './Component';

export default function App() { 
    const dataString = decodeURIComponent(window.location.hash.substring(1));

    let data = null;
    try {
        data = JSON.parse(dataString);
    } catch (e) {}

	return (<div className={styles.appRoot}>
        <Manager
            getPane={() => {
                return <Component />;
            }}
            setData={(newData) => {
                window.location.hash = JSON.stringify(newData);
            }}
            data={data}
        />
	</div>);
}