import React, { useContext } from 'react';
import { ManagerContext } from '@bucky24/window-manager';

export default function Component() {
    const { data, setData } = useContext(ManagerContext);

    const content = [];
    for (let i=1;i<50;i++) {
        const string = "";
        for (let j=0;j<i;j++) {
            string += " line " + i + " ";
        }
        content.push(<div>{string}</div>);
    }

    return (
        <div>
            inner content
            <input
                type="text"
                value={data?.value ?? ''}
                onChange={(e) => {
                    setData({
                        value: e.target.value, 
                    });
                }}
            />
            {content}
        </div>
    );
}