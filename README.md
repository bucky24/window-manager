# bucky24/window-manager

This module is a simple attempt at a user-configurable window manager for React. There are probably better modules out there, so I wouldn't recommend using this in production.

I built this because I like modules with not many dependencies, and I like building things more than I like looking for libraries that already exist.

## Usage

### Manager

The main component exported from this module (as the default export) is the `Manager` component. This component takes no children, but does take the following props:

| Name | Type | Description |
| -- | -- | -- |
| getPane | Function | This method is called once for every pane that exists in the window manager. It can return anything that can be rendered to React. |
| setData | Function | This method is called whenever the data the `Manager` is handling changes. This includes both structural data for the panes themselves and also any data captured by `ManagerContext` for the individual panes |
| data | Object | This is expected to be the same data given from `setData`, used to seed the window manager upon load. Note that this data is only set once, on first load of the component, and any changes after that are ignored. |

```
<Manager
    getPane={() => {
        return <Component />;
    }}
    setData={(newData) => {
        // do something with the data, which is an object
    }}
    data={initialSeedData}
/>
```

### ManagerContext

The ManagerContext is available to any content that is inside a pane. It's meant to provide an easy way of storing some configuration and state data for panes that all contain the same components otherwise. It exports as a value an object containing the following:

| Name | Description |
| -- | -- |
| data | Whatever data the component has previously stored for this given pane. Note that this will start out null. |
| setData | Function that updates the data for this particular pane |

```
export default function Component() {
    const { data, setData } = useContext(ManagerContext);

    return (
        <div>
            inner content
            <input
                type="text"
                value={data?.value ?? ''}
                onChange={(e) => {
                    setData({
                        value: e.target.value, 
                    });
                }}
            />
        </div>
    );
```