import React from 'react';

const ManagerContext = React.createContext({});
export default ManagerContext;

export function ManagerProvider({ children, data, setData }) {
    const value = {
        data,
        setData: (newData) => {
            const finalData = {
                ...data,
                ...newData,
            };
            setData(finalData);
        },
    };
    return (
        <ManagerContext.Provider value={value}>
            {children}
        </ManagerContext.Provider>
    );
}