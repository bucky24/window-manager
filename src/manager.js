import React, { useRef, useState, useEffect } from 'react';

import { ManagerProvider } from './ManagerContext';

const PANE_TYPE = {
    HORIZONTAL: 'pane/horizontal',
    VERTICAL: 'pane/vertical',
};

const useRender = () => {
    const [counter, setCounter] = useState(0);
    
    return () => {
        setCounter((num) => {
            return num + 1;
        });
    }
}

export default function Manager({ getPane, setData, data }) {
    const topPane = useRef({
        type: PANE_TYPE.HORIZONTAL,
        children: [],
    });
    const render = useRender();
    
    useEffect(() => {
        if (data) {
            // need to rebuild the parent relationship
            const process = (obj, parent) => {
                const newObj = {...obj, parent };
                if (newObj.c) {
                    newObj.children = newObj.c.map((child) => {
                        return process(child, newObj);
                    });
                } else {
                    newObj.children = [];
                }
                delete newObj.c;
                
                newObj.type = newObj.t;
                delete newObj.t;
                
                if (newObj.type === 'h') {
                    newObj.type = PANE_TYPE.HORIZONTAL;
                } else if (newObj.type === 'v') {
                    newObj.type = PANE_TYPE.VERTICAL;
                }
                
                newObj.data = newObj.d;
                delete newObj.d;
                
                return newObj;
            }
            const newObj = process(data);
            topPane.current = newObj;
            render();
        }
    }, []);
    
    const updateData = () => {
        if (setData) {
            // we want to backup everything. But before we do, we have to remove the circular references to parent
            const process = (obj) => {
                const newObj = {...obj};
                delete newObj.parent;
                if (newObj.children) {
                    newObj.children = newObj.children.map(process);
                }
                
                if (newObj.type === PANE_TYPE.HORIZONTAL) {
                    newObj.type = 'h';
                } else if (newObj.type === PANE_TYPE.VERTICAL) {
                    newObj.type = 'v';
                }
                
                if (newObj.data === null) {
                    delete newObj.data;
                }
                
                if (newObj.children.length === 0) {
                    delete newObj.children;
                }
                
                if (newObj.children) {
                    newObj.c = newObj.children;
                    delete newObj.children;
                }
                
                if (newObj.data) {
                    newObj.d = newObj.data;
                    delete newObj.data;
                }
                
                newObj.t = newObj.type;
                delete newObj.type;
                
                return newObj;
            }
            const newObj = process(topPane.current);
            setData(newObj);
        }
    }
    
    const renderPane = (pane, key, size = null) => {
        const styles = {
            outline: '1px solid black',
            display: 'flex',
            flexDirection: 'column',
        };

        const containerStyles = {
            width: '100%',
            height: 'calc(100% - 20px)',
        };

        if (pane.children.length > 0) {
            containerStyles.display = 'flex';
        } else {
            containerStyles.flexShrink = '1';
            containerStyles.flexGrow = '0';
            containerStyles.overflowY = 'auto';
        }

        if (pane.type === PANE_TYPE.HORIZONTAL) {
            containerStyles.flexDirection = 'row';
            styles.width = '100%';
            styles.height = size || '100%';
        } else if (pane.type === PANE_TYPE.VERTICAL) {
            containerStyles.flexDirection = 'column';
            styles.width = size || '100%';
            styles.height = '100%';
        } 
        
        return (
            <div
                key={key}
                style={styles}
            >
                <div
                    style={{
                        borderBottom: '1px solid black',
                        display: 'flex',
                    }}
                >
                    <div
                        style={{ cursor: 'pointer', marginRight: 10 }}
                        onClick={() => {
                            const newType = pane.type === PANE_TYPE.VERTICAL ? PANE_TYPE.HORIZONTAL : PANE_TYPE.VERTICAL;
                            pane.children.push({
                                type: newType,
                                children: [],
                                parent: pane,
                            });
                            if (pane.children.length === 1) {
                                pane.children[0].data = pane.data;
                                pane.data = null;
                                pane.children.push({
                                    type: newType,
                                    children: [],
                                    parent: pane,
                                });
                            }
                            updateData();
                            render();
                        }}
                    >
                        Add
                    </div>
                    {pane.parent && (
                        <div
                            style={{
                                cursor: 'pointer',
                            }}
                            onClick={() => {
                                const index = pane.parent.children.indexOf(pane);
                                pane.parent.children.splice(index, 1);
                                if (pane.parent.children.length === 1) {
                                    pane.parent.data = pane.parent.children[0].data;
                                    pane.parent.children.splice(0, 1);
                                }
                                updateData();
                                render();
                            }}
                        >
                            Remove
                        </div>
                    )}
                </div>
                <div
                    style={containerStyles}
                >
                    {pane?.children?.map((child, index) => {
                        return renderPane(child, key + '_' + index, `${100 / pane.children.length}%`);
                    })}
                    {pane?.children?.length === 0 && (
                        <ManagerProvider
                            data={pane.data}
                            setData={(newData) => {
                                pane.data = newData;
                                updateData();
                                render();
                            }}
                        >
                            {getPane()}
                        </ManagerProvider>
                    )}
                </div>
            </div>
        );
    }

    return (
        <div style={{
            width: '100%',
            height: '100%',
        }}>
            {renderPane(topPane.current, '1')}
        </div>
    );
}